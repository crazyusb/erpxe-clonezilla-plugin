#!/bin/bash
set -e
set -x
clonezilladir=/var/lib/tftpboot/er/plugins/clonezilla
arch=x86_64
#echo "Downloading clonezilla"
#wget https://osdn.net/frs/redir.php?m=onet&f=%2Fclonezilla%2F68294%2Fclonezilla-live-20170905-zesty-amd64.iso -O /var/lib/tftpboot/er/plugins/clonezilla/x86_64/
echo "Mounting loop devices to /mnt/cdrom"
mount -o loop /var/lib/tftpboot/er/plugins/clonezilla/x86_64/clonezilla-live-20170905-zesty-amd64.iso /mnt/cdrom
cp /mnt/cdrom/live/{filesystem.*,initrd.img,vmlinuz} $clonezilladir/$arch/
